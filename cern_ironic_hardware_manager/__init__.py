import os
import time
import re

# This try/catch makes code py2/py3 compatible
try:
    # works on py2
    import urllib2 as rq
except ImportError:
    # works on py3
    import urllib.request as rq


from ironic_python_agent import errors, hardware, utils
from ironic_lib import utils as il_utils
from oslo_log import log

LOG = log.getLogger()


class CernHardwareManager(hardware.GenericHardwareManager):
    HARDWARE_MANAGER_NAME = 'cern_hardware_manager'
    HARDWARE_MANAGER_VERSION = '1.01'

    def evaluate_hardware_support(self):
        """Declare level of hardware support provided.

        Since this example covers a case of supporting a specific device,
        this method is where you would do anything needed to initalize that
        device, including loading drivers, and then detect if one exists.

        In some cases, if you expect the hardware to be available on any node
        running this hardware manager, or it's undetectable, you may want to
        return a static value here.

        Be aware all managers' loaded in IPA will run this method before IPA
        performs a lookup or begins heartbeating, so the time needed to
        execute this method will make cleaning and deploying slower.

        :returns: HardwareSupport level for this manager.
        """
        LOG.info("evaluate_hardware_support ()")

        super(CernHardwareManager, self).evaluate_hardware_support()

        # Get IPv4 address of linuxsoft in order to send AIMS deregistration
        # request using IPv4, not IPv6 (as the support of the latter is broken
        # in CERN network infra)

        os.system("sysctl -w net.ipv6.conf.all.disable_ipv6=1")
        os.system("sysctl -w net.ipv6.conf.default.disable_ipv6=1")

        # As AIMS server checks revDNS of the caller, we need to wait here
        # in case we had an user who requested cern-services=False.
        # Otherwise AIMS fails to deregister and we have an endless loop to
        # boot into deploy image

        aims_clients = ["aims.cern.ch", "aims-pdc.cern.ch"]
        for aims_client in aims_clients:
            aims_url = "http://{}/aims/reboot".format(aims_client)
            aims_deregistration = rq.urlopen(aims_url)
            if aims_deregistration.status == 200:
                if "installed by AIMS2 at" in aims_deregistration.read().decode():
                    # All good, machine deregistered
                    LOG.info(aims_deregistration.read().decode())
                    LOG.info("AIMS deregistration succeeded on client {}".format(aims_client))
                    break
                else:
                    # deregistration failed, but we do not care too much:
                    # - on creation, we do not support cern-services=False
                    # - on deletion, we will set up AIMS again
                    # we should be good :)
                    LOG.warning(aims_deregistration.read().decode())
                    LOG.warning("AIMS deregistration didn't succeed on client {}".format(aims_client))
            else:
                LOG.error(aims_deregistration.read().decode())
                LOG.error("Error while AIMS deregistation on client {}".format(aims_client))

        return hardware.HardwareSupport.SERVICE_PROVIDER

    def list_hardware_info(self):
        """Return full hardware inventory as a serializable dict.

        This inventory is sent to Ironic on lookup and to Inspector on
        inspection.

        :returns: A dictionary representing inventory
        """
        hardware_info = super(CernHardwareManager, self).list_hardware_info()
        hardware_info['disk_enclosures'] = self.get_disk_enclosures()
        hardware_info['infiniband_adapters'] = self.get_infiniband_adapters()

        hardware_info['disk_label'] = 'gpt'

        # (makowals) Each value is stored in a separate key as ironic driver prefers to have capabilities
        # without nested jsons, i.e. in a form of key:val
        hardware_info['cpu_name'], hardware_info['cpu_family'], hardware_info['cpu_model'], hardware_info['cpu_stepping'] = self.get_cpu()
        hardware_info['serial_asset_tag'] = self.get_serial_asset_tag()
        return hardware_info

    def get_clean_steps(self, node, ports):
        """Return the clean steps supported by this hardware manager.

        This method returns the clean steps that are supported by
        proliant hardware manager.  This method is invoked on every
        hardware manager by Ironic Python Agent to give this information
        back to Ironic.

        :param node: A dictionary of the node object
        :param ports: A list of dictionaries containing information of ports
                      for the node
        :returns: A list of dictionaries, each item containing the step name,
                  interface and priority for the clean step.
        """
        return [
            {
                'step': 'set_system_time',
                'priority': 40,
                'interface': 'deploy'
            },
            {
                'step': 'delete_configuration',
                'priority': 30,
                'interface': 'deploy',
                'abortable': True,
            },
            {
                'step': 'erase_devices_express',
                'priority': 20,
                'interface': 'deploy'
            },
            {
                'step': 'create_configuration',
                'priority': 10,
                'interface': 'deploy',
                'abortable': True,
            },
            {
                'step': 'wait_a_minute',
                'priority': 1,
                'interface': 'deploy'
            },
            {
                'step': 'erase_devices_metadata',
                'priority': 0,
                'interface': 'deploy'
            },
            {
                'step': 'erase_devices',
                # NOTE(arne): Disabled for now: "shred" takes too long.
                'priority': 0,
                'interface': 'deploy'
            },
            {
                'step': 'check_ipmi_users',
                # NOTE(arne): Disabled for now: needs discussion and sync
                # with procurement team.
                'priority': 0,
                'interface': 'deploy'
            },
            {
                'step': 'burnin_cpu',
                'priority': 0,
                'interface': 'deploy',
                'abortable': True,
            },
            {
                'step': 'burnin_memory',
                'priority': 0,
                'interface': 'deploy',
                'abortable': True,
            },
            {
                'step': 'burnin_disk',
                'priority': 0,
                'interface': 'deploy',
                'abortable': True,
            },
            {
                'step': 'burnin_network',
                'priority': 0,
                'interface': 'deploy',
                'abortable': True,
            },
            {
                'step': 'wait_before_anything',
                # NOTE(dabadaba): Wait for file /tmp/ipa-before-done to exist
                'priority': 0,
                'interface': 'deploy',
                'abortable': True,
            },
            {
                'step': 'wait_after_everything',
                # NOTE(dabadaba): Wait for file /tmp/ipa-after-done to exist
                'priority': 0,
                'interface': 'deploy',
                'abortable': True,
            },
            {
                'step': 'benchmark',
                # NOTE(zmatonis): wget benchmark script and execute it
                'priority': 0,
                'interface': 'deploy',
                'abortable': True,
            }
        ]

    def upgrade_example_device_model1234_firmware(self, node, ports):
        """Upgrade firmware on Example Device Model #1234."""
        # Any commands needed to perform the firmware upgrade should go here.
        # If you plan on actually flashing firmware every cleaning cycle, you
        # should ensure your device will not experience flash exhaustion. A
        # good practice in some environments would be to check the firmware
        # version against a constant in the code, and noop the method if an
        # upgrade is not needed.

        def _is_latest_firmware():
            """Detect if device is running latest firmware."""
            # Actually detect the firmware version instead of returning here.
            node.get('created_at')
            return True

        def _upgrade_firmware():
            """Upgrade firmware on device."""
            # Actually perform firmware upgrade instead of returning here.
            return True

        if _is_latest_firmware():
            LOG.debug('Latest firmware already flashed, skipping')
            # Return values are ignored here on success
            return True
        else:
            LOG.debug('Firmware version X found, upgrading to Y')
            # Perform firmware upgrade.
            try:
                _upgrade_firmware()
            except Exception as e:
                # Log and pass through the exception so cleaning will fail
                LOG.exception(e)
                raise
            return True

    def erase_devices(self, node, ports):
        """Erase any device that holds user data.

        This method in its current state will erase all block devices using
        either ATA Secure Erase or shred, depending on the system capabilities.
        """
        super(CernHardwareManager, self).erase_devices(node, ports)

    def erase_devices_metadata(self, node, ports):
        """Attempt to erase the disk devices metadata."""
        super(CernHardwareManager, self).erase_devices_metadata(node, ports)

    def wait_a_minute(self, node, ports):
        """Holds the IPA for a minute after automatic cleaning to inspect logs.

        :param node: A dictionary of the node object
        :param ports: A list of dictionaries containing information of ports
                      for the node
        """
        LOG.warning("Waiting 60 seconds for log inspection ....")
        time.sleep(60)

    def set_system_time(self, node, ports):
        """Set the system time to be synchronised

        :param node: A dictionary of the node object
        :param ports: A list of dictionaries containing information of ports
                      for the node
        """
        LOG.info("Setting the system time to avoid inconsistencies")
        time_servers = 'ip-time-1.cern.ch ip-time-2.cern.ch'
        utils.execute("/usr/sbin/ntpdate -sub {} || :".format(time_servers), shell=True)
        utils.execute("[ -x /sbin/clock ] && /sbin/clock --systohc || :", shell=True)

    def wait_before_anything(self, node, ports):
        """Holds the IPA until the /tmp/ipa-before-done is present.

        :param node: A dictionary of the node object
        :param ports: A list of dictionaries containing information of ports
                      for the node
        """
        while not os.path.isfile("/tmp/ipa-before-done"):
            LOG.info("Waiting until /tmp/ipa-before-done exists ...")
            time.sleep(30)

    def wait_after_everything(self, node, ports):
        """Holds the IPA until the /tmp/ipa-after-done is present.

        :param node: A dictionary of the node object
        :param ports: A list of dictionaries containing information of ports
                      for the node
        """
        while not os.path.isfile("/tmp/ipa-after-done"):
            LOG.info("Waiting until /tmp/ipa-after-done exists ...")
            time.sleep(30)

    def benchmark(self, node, ports):
        """curl benchmarking script and executes it
        :param node: A dictionary of the node object
        :param ports: A list of dictionaries containing information of ports
                      for the node
        """
        LOG.info("Executing benchmark step")
        # this will put extra variables set on the node and set them as
        # environment variables before execute script
        if 'extra' in node and 'SCRIPT_URL' in node['extra'] \
                and len(node['extra']['SCRIPT_URL']) != 0:
            script_location = node['extra']['SCRIPT_URL']
        else:
            raise errors.CleaningError(
                "ERROR: extra.SCRIPT_URL is not properly set"
            )
        env_var = ""
        if 'resource_class' in node:
            env_var += " export NODE_RESOURCE_CLASS={0} ;"\
                .format(node['resource_class'])
        if 'name' in node:
            env_var += " export NODE_NAME={0} ;".format(node['name'])
        if 'extra' in node and 'ENV_VAR' in node['extra']:
            # check if 'ENV_VAR' are passed in correct <key=value> format,
            # we substring words matching regex and then remove empty spaces
            # in case there is still something left, variables vere not passed
            # correctly and we fail
            regex = r'(\w+)=([^\s]+)'
            extra_env_var = node['extra']['ENV_VAR']
            if len(re.sub(regex, '', extra_env_var).replace(' ', '')) != 0:
                LOG.error("extra.ENV_VAR is not properly set: {0}".format(
                    extra_env_var))
                raise Exception("extra.ENV_VAR is not properly set,"
                                " check IPA logs for more info.")
            env_var += "export {0} ;".format(extra_env_var)
        # NOTE: since we are calling a bash script which calls docker in
        # privileged mode,there is little to no incentive to try and secure it
        script = "{0} curl -s {1} | bash -ex > " \
                 "/tmp/ironic_script_debug.txt 2>&1"\
            .format(env_var, script_location)
        LOG.info("Executing script : '" + script + "'")
        result = il_utils.try_execute(script, shell=True)
        if result is None:
            raise errors.CleaningError(
                "ERROR: There was a cleaning error "
                "executing benchmark script: {}"
                .format(script_location)
            )

    def check_ipmi_users(self, node, ports):
        """Check users having IPMI access with admin rights

        In CERN environment there should be only 2 users having admin access
        to the IPMI interface. One of them is node.driver_info["ipmi_username"]
        and the other is admin/root.

        As the superadmin's username is not known beforehand, if we detect >2
        users, cleaning should fail. In future we may want to implement logic
        to automatically delete any unnecessary user from IPMI.
        """
        for channel in range(16):
            # Count number of enabled admin users
            out, e = utils.execute(
                "ipmitool user list {0!s} | awk '{{if ($3 == \"true\" && $6 == \"ADMINISTRATOR\") print $0;}}' | wc -l".format(channel + 1), shell=True)
            if int(out) != 1:
                raise errors.CleaningError("Detected {} admin users for IPMI !".format(out))

            # In case there is only 1 ipmi user, check if name matches the one
            # known by Ironic
            out, e = utils.execute(
                "ipmitool user list {0!s} | awk '{{if ($3 == \"true\" && $6 == \"ADMINISTRATOR\") print $2;}}' | wc -l".format(channel + 1), shell=True)
            if out != node.get('driver_info')['ipmi_username']:
                raise errors.CleaningError("Detected illegal admin user \"{}\" for IPMI !".format(out))

            # The following error message indicates we started querying
            # non existing channel
            if "Get User Access command failed" in e:
                break

    def get_disk_enclosures(self):
        """Detect number of external drive enclosures

        Used by list_hardware_info to populate node's properties with a number
        of external arrays connected to the device. Please note this assumes
        all the drivers required to detect the array have been loaded
        beforehand.

        :returns: A number of external arrays
        """
        # TODO(makowals): Check if the drivers are loaded before doing lsscsi;
        # we should compile a list of potentials modules we want to have
        # and then use utils.try_execute('modprobe', 'xyz')

        out, e = il_utils.execute("lsscsi | grep enclosu | wc -l", shell=True)
        return int(out)

    def get_infiniband_adapters(self):
        """Detect number of infiniband network adapters

        Used by list_hardware_info to populate node's properties with a number
        of infiniband adapters connected to the device. Please note this
        assumes all the drivers required to detect the device have been loaded
        beforehand.

        :returns: A number of infiniband network adapters
        """

        out, e = utils.execute("ibv_devinfo | awk '/transport[[:space:]]*:/ {{print $2}}' | grep InfiniBand | wc -l", shell=True)
        return int(out)

    def get_cpu(self):
        result = {}

        result['cpu_name'], e = utils.execute("lscpu | awk '/Model name[[:space:]]*:/ {{$1=$2=\"\"; print $0}}'", shell=True)
        result['cpu_family'], e = utils.execute("lscpu | awk '/CPU family[[:space:]]*:/ {{print $3}}'", shell=True)
        result['cpu_model'], e = utils.execute("lscpu | awk '/Model[[:space:]]*:/ {{print $2}}'", shell=True)
        result['cpu_stepping'], e = utils.execute("lscpu | awk '/Stepping[[:space:]]*:/ {{print $2}}'", shell=True)

        result = {k: v.strip() for k, v in list(result.items())}

        return result['cpu_name'], result['cpu_family'], result['cpu_model'], result['cpu_stepping']

    def get_serial_asset_tag(self):
        serial, asset_tag = "", ""
        try:
            out = utils.execute('ipmitool', 'fru', 'print', '0', use_standard_locale=True)
        except Exception as e:
            LOG.info("ipmitool command failed to obtain requested data.")
            LOG.exception(e)
            raise
        lines = out[0].splitlines()
        for line in lines:
            if 'Product Serial' in line:
                serial = line.split(':')[1].strip()
            if 'Product Asset Tag' in line:
                asset_tag = line.split(':')[1].strip()

        return((asset_tag + '-' + serial).lower())
